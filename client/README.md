Document all concepts and your implementation decisions.
###ParseData Function
The parse data function takes the company and their information and puts it into the array
series.  It also checks if there is already an array for the company.

###retrieveRows Function
This function passes the object to a string as a JSON object

###showData Function
This function returns the data to the DOM so that it will display. It adds the data to a
table in the DOM. It also checks if the values are positive or negative so that styling
can be applied to them.

###generateTestData function
This function generates fake data

###getRealTimeData function
This function takes data from a web socket

###loop Function
This function calls the functions that show data and get data from ajax.
It also removes any old data and refreshes it with the new data.

###initHTML Function
This function creates HTML elements and their id's so that they can be styled in the
stylesheet.

#Flow of the program
TODO: Improve this flow
1. init
1. retrieve data
1. draw data
1. go to step 2


#Concepts
For every concept the following items:
- short description
- code example
- reference to mandatory documentation
- reference to alternative documentation (authoritive and authentic)

###Objects, including object creation and inheritance
An object in JavaScript has properties and methods.  Each object of the same type
 shares these, but their values for the properties are different.

 To create an object:
 var exampleObject ={type: "normal", color: "black", size: "medium"};

 In the assignment we create an object for companies and at it to an array.
 We do this for each company.

 Inheriting Methods:
 var anotherExample = Object.create(exampleObject);
 The anotherExample object will inherit methods from exampleObject

###websockets
A websocket creates a connection between a web browser and a server.
Both can send data at anytime.

var websocketExample = new WebSocket('ws://test.com', ['test', 'xmpp']);
###XMLHttpRequest
An object that provides an easy way to retrieve data from a URL without having to
 refresh a whole page

 var exampleRequest = new XMLHttpRequest();

###AJAX
Exchanges data with a server to update parts of a webpage without having to reload
the whole page.

 {
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }

###Callbacks
Callback functions are functions that can be executed within other function only after
another function is executed.

function do_a(){
  console.log( '`do_a`: this comes out first');
}

function do_b(){
  console.log( '`do_b`: this comes out later' );
}

do_a();
do_b();

Source: http://dreamerslab.com/blog/en/javascript-callbacks/

###How to write testable code for unit-tests
source: http://www.htmlgoodies.com/beyond/javascript/testing-javascript-using-the-jasmine-framework.html
