/*jslint browser: true, regexp: true */
/*global app, data, io */

(function () {
    "use strict";
    window.app = {

        series: {},

        socket: io("http://server7.tezzt.nl:1333"),

        settings: {
            refresh: 1000,
            ajaxUrl: "http://server7.tezzt.nl/~theotheu/stockquotes/index.php"
        },


        parseData: function (rows) {
            var i, company;

            // Iterate over the rows and add to series
            for (i = 0; i < rows.length; i++) {
                company = rows[i].col0;

                // Check if array for company exist in series
                if (app.series[company] !== undefined) {
                    app.series[company].unshift(rows[i]);
                } else {
                    // company does not yet exist
                    app.series[company] = [rows[i]];
                }
            }

        },


        retrieveRows: function (e) {
            var str = e.target.responseText;
            //app.addToSeries(JSON.parse(str));
            var obj = JSON.parse(str);
            var rows = obj.request.results.row;
            app.addToSeries(rows);
        },

        createValidCSSNameFromCompany: function (str) {
            return str.replace(/[^a-zA-Z0-9]/g, "");
        },

        showData: function () {
            //return value is a dom
            var company, table, row, quote, cell, propertyName, propertyValue;


            //create table
            table = document.createElement("table");
            table.id = "myTable";

            //create rows
            for (company in app.series) {
                quote = app.series[company][0];
                row = document.createElement("tr");
                row.id = app.createValidCSSNameFromCompany(company);

                //create cells
                table.appendChild(row);

                //iterate over quote to create cells
                for (propertyName in quote) {
                    propertyValue = quote[propertyName];
                    cell = document.createElement("td");
                    cell.innerText = propertyValue;
                    row.appendChild(cell);
                }
                //adds class names to rows, so that it can be styled accordingly
                if (quote.col4 < 0) {
                    row.className = 'loser';
                } else if (quote.col4 > 0) {
                    row.className = 'winner';
                } else if (quote.col4 === 0) {
                    row.className = 'neutral';
                }
            }

            return table;
        },

        rnd: function (input, range) {
            // return a value between min and max,
            // eg. if the current value is 100,
            // then the new value will be a float between
            // 100-min, 100+max

            var min, max;

            max = input + range;
            min = input - range;

            return Math.floor(
                    Math.random() * (max - min + 1)
                ) + min;
        },

        getFormattedDate: function (date) {
            // Return a formatted date string that looks like
            // month/day/year
            // year is 4 digits

            var dt = new Date();

            return (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear();
        },

        getFormattedTime: function (date) {
            // am, pm
            var minutes = date.getMinutes();

            if (minutes < 10) {
                minutes = "0" + minutes;
            }

            return (date.getHours() + ":" + minutes);
        },

        generateTestData: function () {
            var company, quote, newQuote;

            for (company in app.series) {
                quote = app.series[company][0];
                newQuote = Object.create(quote);
                newQuote.col0 = app.createValidCSSNameFromCompany(quote.col0); // gets company names from data.js
                newQuote.col1 = app.rnd(1, 100); // new value
                newQuote.col2 = app.getFormattedDate(new Date()); // new date
                newQuote.col3 = app.getFormattedTime(new Date()); // new time
                newQuote.col4 = app.rnd(1, 100); // difference of price value between this one and the previous quote
                newQuote.col5 = 'N/A';
                newQuote.col6 = 'N/A';
                newQuote.col7 = 'N/A';
                newQuote.col8 = app.rnd(1000, 1000);

                app.addToSeries(company, newQuote);

            }
        },

        getRealTimeData: function () {
            app.socket.on("stockquotes", function (data) {
                app.parseData(data.query.results.row);
            });
        },

        addToSeries: function (q) {
            app.series[q].unshift(q);
            if (app.series[q].length > app.settings.dataPoints) {
              app.series.shift();
            }},

        loop: function () {
            var table;
            //app.getDataFromAjax();
            table = app.showData();

            //setTimeout(app.loop,
            //app.settings.refresh);


            app.generateTestData();

            //remove old table
            if (document.querySelector("#myTable")) {
                 document.querySelector("#container")
                    .removeChild(document.querySelector("#myTable"));
            }

            app.container.appendChild(table);

            setTimeout(app.loop, app.settings.refresh)

           // app.series[company].[app.series[company].length]

        },

        initHTML: function () {
            var container, h1Node, table;

            // Create container
            container = document.createElement("div");
            container.id = "container";

            app.container = container;


            table = document.createElement("div");
            table.id = "table";

            // Create title of application
            h1Node = document.createElement("h1");
            h1Node.innerText = "Real Time Stockquote App";

            app.container.appendChild(h1Node);

            return app.container;
        },

        // TODO: add method for retrieving data from URL
        // TODO: add checks
        getDataFromAjax: function () {
            var xhr;
            xhr = new XMLHttpRequest();
            xhr.open("GET", app.settings.ajaxUrl);
            xhr.addEventListener("load", app.retrieveJSON);
            xhr.send();
            //app.addToSeries();
        },

        retrieveJSON: function (e) {
            //app.parseData(JSON.parse(e.target.responseText));
            var str = e.target.responseText;
            //app.addToSeries(JSON.parse(str));
            var obj = JSON.parse(str);
            //console.log(obj);
            var rows = obj.query.results.row;
            app.parseData(rows);
        },


        init: function () {

            var container;

            // Add HTML to page
            container = app.initHTML();
            document.querySelector("body").appendChild(container);

            // Parse initial data
            app.parseData(data.query.results.row);
            app.getRealTimeData();

            app.loop();

            //     app.generateTestData();
            /*table = app.showData();
             app.container.appendChild(table);

             app.generateTestData();

             alert("Before");
             document.querySelector('#container')
             .removeChild(
             document.querySelector("table")
             );
             alert("After");

             table=app.showData();
             app.container.appendChild(table);*/


        }

    }
}());