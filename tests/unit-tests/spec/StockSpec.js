/*jslint browser: true, plusplus:true */

describe("scenario", function () {

    it("assertion", function () {
        expect(1).toBe(1);
    });

});


describe("html tests", function () {

    var properties, methods;

    beforeEach(function () {
        var propertyName;

        app.init();

        properties = [];
        methods = [];
        for (propertyName in app) {
            if (app.hasOwnProperty(propertyName)) {
                if (app[propertyName].constructor === Function) {
                    methods.push(app[propertyName].prototype);
                } else {
                    properties.push(propertyName);
                }
            }
        }
    });

    afterEach(function () {
        document.body.removeChild(document.querySelector("#container"));
    });

    it("Should verify that the app has properties and methods.", function () {
        expect(properties.length).not.toBe(0);
        expect(methods.length).not.toBe(0);

    });


    it("Should verify that DOM elements are created for the container and the title", function () {
        var expectedValue = 'Real Time Stockquote App';
        var actualValue = app.initHTML().querySelector("h1").innerText;

        expect(actualValue).toBe(expectedValue);

    });

    it("Should verify that the table has 25 rows", function () {
        var actualValue = app.showData().querySelectorAll("tr").length;
        expect(actualValue).toBe(25)
    });


    it("Should verify that only letters and numbers remain", function () {
        var actualValue = app.createValidCSSNameFromCompany("HGF>&^#%%$#@");
        var expectedValue = "HGF";
        expect(actualValue).toBe(expectedValue);
    });

    it("Should verify that the function outputs a random value between the max and min", function(){

        var input = 100, range = 5, min = 0, max = 0, i, r;

        for (i = 0; i < 1000; i++) {
            r = app.rnd(input, range);
            if (r === input - range) {
                min++;
            } else if (r === input + range) {
                max++;
            }
            }
        expect(min).toBeEqualTo(0);
        expect(max).toBeEqualTo(0);

    });

    it("Should verify that dates are formatted as DD/MM/YYYY", function(){

    });

    it("Should verify that time is formatted", function(){

    });

    it("Should verify that test data is properly generated", function(){

    });

});